import unittest
import sys
import os

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '../src')))

import ObjectForTest

class TestImpl(unittest.TestCase):

    def test_passtest(self):
        self.assertEqual(ObjectForTest.add(2, 2), 4)

    def test_failtest(self):
        self.assertEqual(ObjectForTest.add(2, 3), 5)

if __name__ == '__main__':
    unittest.main()