import org.scalatest._

class Test extends FlatSpec{
  "2 add 2" must "be equals 4" in {
    val oft = new ObjectForTest()
    assert(oft.add(2, 2) === 4)
  }
}
